const form = document.getElementById("searchWord");
const iconSearch = document.getElementById("searchIconContainer");
const searchInput = document.getElementById("search");
let word = document.getElementById("word");
let partOfSpeech = document.getElementById("partOfSpeech");
let soundIcon = document.getElementsByClassName("soundIcon");
let resultSection = document.getElementById("resultSection");

eventListeners();

function eventListeners() {
  form.addEventListener("submit", searchWord);
  iconSearch.addEventListener("click", searchWord);
  // document.addEventListener('load', historyUI)
}
historyUI();

function searchWord(e) {
  e.preventDefault();
  let word = searchInput.value.trim();
  makeApiCall(word);
  // console.log(word);
}

function playAudio(url) {
  new Audio(url).play();
}

function getLocalSTR(params) {
  let arr;
  let newWord = setLocalStorage(params);
  
  if(!localStorage.getItem('words')){
    arr = [];
    newWord = setLocalStorage(params);
  }else{
    arr = JSON.parse(localStorage.getItem('words'));
    newWord = setLocalStorage(params);
  }
  arr.push(newWord);
  localStorage.setItem('words', JSON.stringify(arr));
}
class Word{
  constructor(word, definition, partOfSpeech){
    this.word = word;
    this.definition = definition;
    this.partOfSpeech = partOfSpeech;
  }
}
function setLocalStorage(param){
  let word = new Word(param[0].word, param[0].meanings[0].definitions[0].definition , param[0].meanings[0].partOfSpeech)
  // console.log(word);
  return word;
}

async function makeApiCall(word) {
  const BASE_URL = "https://api.dictionaryapi.dev/api/v2/entries/en/";
  let result = await axios
    .get(`${BASE_URL}${word}`)
    .then((res) => {
      return res;
    })
    .catch((err) => err);

  let resultData;
  resultSection.innerHTML = ``;
  if (result.status == 200) {
    resultData = result.data;
    // console.log("resultData>>>>>>>>>>>>>>>>>>", resultData);
    getLocalSTR(resultData);
    resultData.map((word, index) => {
      // console.log("map>>>>works");
      // let audiUrl ='';
      resultSection.innerHTML += `
          <div class="card m-auto " style="border: none">
            ${word.meanings.map((element) => {
              // console.log(element);
              return `
              <div class="d-flex">
                <h5 id="word" class="card-title marginR12">${word.word}</h5>
                <div id="partOfSpeech" class="d-flex">
                  <span class="marginR12">${element.partOfSpeech}</span> <br>
          
                  <div class="soundIcon ml-3 marginR12" onclick="playAudio('${word.phonetics[0].audio}')">
                      <i class="fa-light fa-volume"></i>
                  </div>
                  <br/>
                  <span class="marginR12">${word.phonetic}</span>
                </div>
              </div>
              ${element.definitions.map((defEl) => {
                return `
                  <div class="card-body">
                    <p id="definition" class="card-text">${defEl.definition}</p>
                    ${
                      (defEl.example)
                        ? `
                    <ul>
                        <li>
                            <p id="example" class="card-text fst-italic">${defEl.example}</p>
                        </li>
                    </ul>
                    `
                        : ``
                    }
                  </div>
                    ${
                    (defEl.synonyms.length == 0)
                      ? ``
                      : `
                    <div class="synon mb-4">
                        <p>Synonyms</p>
                        <div id="syns-container">
                            ${defEl.synonyms.map((synon) => {
                              return `<span class="py-2 px-3 border rounded-pill">${synon}</span>`;
                            })}
                        </div>
                    </div>
                    `
                      }
                    ${
                      (defEl.antonyms.length == 0)
                        ? ``
                        : `
                    <div class="anton mb-4">
                        <p>Antonyms</p>
                        <div id="anton-container">
                            ${defEl.antonyms.map((antonym) => {
                              return `<span class="py-2 px-3 border rounded-pill">${antonym}</span> <br/>`;
                            })}
                        </div>
                    </div>
                    `
                    }
                
              `
            })}
              `
            })}
          </div>
        `
    });
  } else {
    // console.log(result.message);
    resultData = "Word doesn't exist";
    resultSection.innerHTML = resultData;
  }
  // console.log("resultData>>>>>>>>>>>>>>>", resultData);
  historyUI();
}


function historyUI(){
  let allHistoryUI = document.getElementById('allHistory');
  allHistoryUI.innerHTML = '';
  let arr = JSON.parse(localStorage.getItem('words'));
  arr.reverse();
  // console.log(arr);
  arr.map(element => {
    allHistoryUI.innerHTML += `
        <div class="row pb-4 m-0" style="width: 100%;">
          <div class="card m-auto" style="border: 0;">
            <div class="card-body">
                <h5 class="card-title">${element.word}</h5>
                <div class="d-flex">
                    <span>${element.partOfSpeech}</span>
                    
                </div>
                <p id="meaning" class="card-text">${element.definition}</p>
            </div>
          </div>
        </div>
    `
  })
 
}